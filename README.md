CLI Tools for the Phalcon Framework
===================================


An expansion to the Phalcon Frameworks CLI Classes. This includes things like Questions, Confirmation, Command test class, Input/Output Streams and Application wrapper that allows you to start a CLI with minimal Effort.

## Setting up your application

Setting up your CLI app is easy, heres a little example:

	set_time_limit(0);
    
    require_once __DIR__ . '/../vendor/autoload.php';
    require_once __DIR__ . '/../app/tasks/BasicTask.php';

    use Vht\Cli\Application;
    
    $app = new Application;

    // Add your Tasks
    $app->add(new BasicTask());

    try {

        $app->start($argv);

    } catch(\Exception $e) {

        echo $e->getMessage();
        exit(255);
    }
	
Want to use your own DI instance? cool:

	#!/usr/bin/env php
	<?php 

	$di = new Phalcon\DI;
	$app = new Vht\Cli\Application($di);

	$app->add(new Task);

	$app->start($argv);

See the documentation below for more details, how to create task classes, setup argument and option variables and more...

## Example

    cd vht-phalcon-cli
    php app/cli basic
    php app/cli basic:other

## Documentation

 - [Installation](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Installation.md)
 - [Writing Tasks](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Writing%20Tasks.md)
 - [Working with params](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Working%20With%20Params.md)
 - [Arguments and Options](https://github.com/Danzabar/phalcon-cli/blob/master/docs/InputArgumentInputOption.md)
 - [Input Output](https://github.com/Danzabar/phalcon-cli/blob/master/docs/InputOutput.md)
 - [Helpers](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Helpers.md)
 - [Questions](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Questions.md)
 - [Confirmation](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Confirmation.md)
 - [Tables](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Tables.md)
 - [Format](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Formats.md)
 - [Testing Commands](https://github.com/Danzabar/phalcon-cli/blob/master/docs/Testing%20Commands.md) 
