<?php
    
    Class BasicTask extends \Vht\Cli\Tasks\Tasks
    {
        // This will be the task name defined in the library.
        protected $name = 'basic';

        protected $description = 'A basic task that does pretty much nothing';

        /**
         * Every task should have a main method, it will be the default
         * action that is called if no other is specified.
         *
         * We use the Annotation engine to specify which commands are actions. Make sure you include it on every action!
         *
         * @Action
         */
        public function main()
        {
            $this->output->writeln("This is the main action");
        }

        /**
         * @Action
         */
        public function other()
        {
            $this->output->writeln("This is the other action");
        }

    }